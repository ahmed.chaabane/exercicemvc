<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<head>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons">
</head>
<body>
    <div class="container">
    	<div class="title">
    		<h3>ARTISTES</h3>
    	</div>
    	<div class="row">
            <div class="col-md-12">
                <h4>Liste des Artistes</h4>
            </div>
            <div class="col-lg-8 col-md-10 ml-auto mr-auto">
                <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">Photo</th>
                            
                            <th>Nom</th>
                            <th>Prenom</th>
                            <th>code</th>
                            <th class="text-right">pays</th>
                          <th class="text-right">id domaine</th> 
                            <th class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    include_once("artiste/modele/liste.php");
while ($donnees = $reponse->fetch()) {
    ?>     
  
                         <tr>
			  
<td><?php echo "<img src='".$donnees['photo']."' width='100px' height='100px'>"; ?></td>
			
			<td><?php echo $donnees['nom'];?></td>
			<td><?php echo $donnees['prenom'];?></td>
			<td><?php echo $donnees['code'];?></td>
			<td><?php echo $donnees['pays'];?></td>
			<td><?php echo $donnees['iddomaine'];?></td>
                            <td class="td-actions text-right">
                            <a href="index.php?composant=<?php echo $_GET['composant'];?>&page=detail&action=new">
                                <button type="submit" name="submit" rel="tooltip" class="btn btn-info btn-just-icon btn-sm" data-original-title="" title="">
                                    <i class="material-icons">person</i>
                                   
                                </button>
                                </a>
                                </td>
                                <td>
                                <a href="index.php?composant=<?php echo $_GET['composant']?>&page=detail&action=update&idartiste=<?php echo  $donnees['idartiste']; ?>">
                                <button type="submit" rel="tooltip" class="btn btn-success btn-just-icon btn-sm" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                </button></a>
                                </td>
                                <td>
                                <button type="submit" rel="tooltip" class="btn btn-danger btn-just-icon btn-sm" data-original-title="" title="">
                                    <i class="material-icons">close</i>
                                </button>
                            </td>
                        </tr>
                        
           
<?php

}
$reponse->closeCursor();

?>
           
               </body>
               