<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
a.btn, .btn {
    position: relative;
    overflow: hidden;
    display: inline-block;
    vertical-align: middle;
    height: 42px;
    line-height: 42px;
    padding: 0;
    font-weight: 400;
    text-align: center;
    color: #fefefe;
    font-size: 14px;
    background: #304fff;
    cursor: pointer;
    transition: all 0.2s ease 0s;
    -moz-transition: all 0.2s ease 0s;
    -webkit-transition: all 0.2s ease 0s;
    -o-transition: all 0.2s ease 0s;
    border-radius: 2px;
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    -khtml-border-radius: 2px;
}


a.btn .circle, .btn .circle {
    padding: 0 30px;
}
.btn_animated .circle {
    width: 100%;
    height: 100%;
    display: block;
}
.animate {
    -webkit-animation: ripple 0.6s linear;
    animation: ripple 0.6s linear;
}

.ink {
    display: block;
    position: absolute;
    background: rgba(0, 0, 0, 0.2);
    border-radius: 100%;
    transform: scale(0);
    -webkit-transform: scale(0);
    -moz-transform: scale(0);
    -o-transform: scale(0);
}



@keyframes ripple {
	100% {
	opacity: 0;
	transform: scale(4);
	-webkit-transform: scale(4);
	}

}


li{
    list-style-type:none;
}
</style>
<br>
<div class="container">
	<div class="row">
	<div class="bts align-center">
	 <a href ="index.php?composant=artiste" class="btn btn_animated">
	 <span class="circle">
	     <span class="ink animate" style="height: 123px; width: 123px; top: -33.9375px; left: 61.0625px;"></span>gestion des artistes 
	  </span>
	 </a>
	</div>
	<br>
	<br><hr>
	<div class="container">
	<div class="row">
	<div class="bts align-center">
	<a href="index.php?composant=exposition" class="btn btn_animated">
	 <span class="circle">
	     <span class="ink animate" style="height: 123px; width: 123px; top: -33.9375px; left: 61.0625px;"></span>gestion des expositions 
	  </span>
	 </a>
	</div>
	<br>

		<br><hr>
	<div class="container">
	<div class="row">
	<div class="bts align-center">
	<a href ="index.php?composant=oeuvre" class="btn btn_animated">
	 <span class="circle">
	     <span class="ink animate" style="height: 123px; width: 123px; top: -33.9375px; left: 61.0625px;"></span>gestion des chefs d'oeuvres 
	  </span>
	 </a>
	</div>
	
	
  </div>
</div>

